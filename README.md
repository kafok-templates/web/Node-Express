# Node.js con Express.js
Esta plantilla es un proyecto pre-configurado en Node.js con Express.js.

# ¿Cómo usar esta plantilla?
Para usar esta plantilla haga lo siguiente:
1. Clonese este repositorio, donde *NOMBRE_PROYECTO* será el nombre que le desee dar al proyecto:
```
git clone https://gitlab.com/kafok-templates/web/Node-Express.git NOMBRE_PROYECTO
cd NOMBRE_PROYECTO
```

2. Ahora borre la carpeta *NOMBRE_PROYECTO/.git* para reiniciar su repositorio.
3. Cree su repositorio de nuevo, esta vez limpio:
```
git init
git add .
git commit -m "Primer commit"
```

4. En caso de que quiera añadir cualquier modificación que se encuentre en otra rama, antes del paso dos haremos:
```
git pull origin NOMBRE_DE_LA_RAMA
```

5. Elimine los archivos `README.md`, `.gitlab-ci.yml` y `LICENCE` o sustitúyalos por los suyos propios.
6. Instale las dependencias de node:
```
npm install
```

